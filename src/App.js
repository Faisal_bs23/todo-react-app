import React, { useState } from "react";
import TodoList from "./components/TodoList";
import ModalForm from "./components/ModalForm";
import "bootstrap/dist/css/bootstrap.css";
import { Container } from "react-bootstrap";
import "./App.css";

function App() {
  const [todos, setTodos] = useState([]);
  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [formHeading, setFormHeading] = useState("Add");
  const [todo, setTodo] = useState({});
  const [filterBy, setFilterBy] = useState("All");

  const todo_obj = {
    todo,
    show,
    todos,
    edit,
    formHeading,
    filterBy,
    setTodo,
    setShow,
    setTodos,
    setEdit,
    setFormHeading,
    setFilterBy,
  };

  return (
    <div className="App">
      <Container>
        <h4>My Todo App</h4>
        <hr />
        <button className="btn btn-primary" onClick={() => setShow(true)}>
          Create New
        </button>
        <ModalForm todo_obj={todo_obj} />
        <br />
        <TodoList todo_obj={todo_obj} />
      </Container>
    </div>
  );
}

export default App;
