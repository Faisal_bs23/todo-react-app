import React from "react";
import { Form } from "react-bootstrap";

const FilterByStatus = (props) => {
  const { filterBy, setFilterBy } = props.todo_obj;

  return (
    <div>
      <Form>
        <Form.Group className="mb-3" controlId="status">
          <span className="boldFont">Filter By: </span>
          <input
            type="radio"
            id="Active"
            value="Active"
            checked={filterBy === "Active"}
            onChange={(e) => {
              // console.log(e.target.value);
              setFilterBy(e.target.value);
            }}
          />
           
          <label htmlFor="Active" className="filtersMargin">
            Active{" "}
          </label>
          <input
            type="radio"
            id="Done"
            value="Done"
            checked={filterBy === "Done"}
            onChange={(e) => {
              setFilterBy(e.target.value);
            }}
          />
           
          <label htmlFor="Done" className="filtersMargin">
            {" "}
            Done
          </label>
          <input
            type="radio"
            id="All"
            value="All"
            checked={filterBy === "All"}
            onChange={(e) => {
              setFilterBy(e.target.value);
            }}
          />
           
          <label htmlFor="All" className="filtersMargin">
            {" "}
            All
          </label>
        </Form.Group>
      </Form>
    </div>
  );
};

export default FilterByStatus;
