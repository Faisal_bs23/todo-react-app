import React from "react";
import { Button, Form, Modal } from "react-bootstrap";

const ModalForm = (props) => {
  const {
    show,
    setShow,
    todos,
    setTodos,
    formHeading,
    edit,
    setEdit,
    todo,
    setTodo,
    setFormHeading,
  } = props.todo_obj;

  const handleSubmit = (e) => {
    e.preventDefault();

    const new_todo = {
      id: todos.length + 1,
      title: todo.title,
      description: todo.description,
      status: todo.status || "Active",
    };

    if (!edit) {
      setTodos([...todos, new_todo]);
      setTodo({});
    } else {
      todos.map((_todo) => {
        if (_todo.id === todo.id) {
          _todo.title = todo.title;
          _todo.description = todo.description;
          _todo.status = todo.status;
        }
        return _todo;
      });

      setEdit(false);
      setFormHeading("Add");
      setTodo({});
    }

    setShow(false);
  };

  return (
    <div>
      <Modal show={show} animation={false}>
        <Modal.Body>
          <h2>{formHeading}</h2>
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="title">
              <Form.Label>Title</Form.Label>
              <Form.Control
                value={todo.title || ""}
                onChange={(e) => setTodo({ ...todo, title: e.target.value })}
                type="text"
                placeholder="Enter title"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter a description"
                value={todo.description || ""}
                onChange={(e) =>
                  setTodo({ ...todo, description: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="status">
              <input
                type="radio"
                id="Active"
                value="Active"
                checked={todo.status === "Active"}
                onChange={(e) => setTodo({ ...todo, status: e.target.value })}
              />
               <label htmlFor="Active">Active</label>
              <input
                type="radio"
                id="Done"
                value="Done"
                checked={todo.status === "Done"}
                onChange={(e) => {
                  setTodo({ ...todo, status: e.target.value });
                }}
              />
               <label htmlFor="Done">Done</label>
            </Form.Group>

            <Button type="submit" className="btn btn-primary todo-buttons">
              {formHeading}
            </Button>

            <Button
              className="btn btn-primary todo-buttons"
              onClick={() => {
                setShow(false);
                setTodo({});
              }}
            >
              Cancel
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default ModalForm;
