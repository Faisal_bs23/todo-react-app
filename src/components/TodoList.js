import React, { useState } from "react";
import ModalForm from "./ModalForm";
import DeleteForm from "./DeleteForm";
import FilterByStatus from "./FilterByStatus";

const TodoList = (props) => {
  const {
    todos,
    // todo,
    edit,
    setEdit,
    setShow,
    setFormHeading,
    setTodo,
    filterBy,
  } = props.todo_obj;

  const [deleteForm, setDeleteForm] = useState(false);

  const new_todos =
    filterBy === "All"
      ? todos
      : todos.filter((_todo) => _todo.status === filterBy);

  const setNewTodo = (_todo) => setTodo({ ..._todo });

  const handleEdit = (_todo) => {
    setEdit(true);
    setShow(true);
    setFormHeading("Edit");
    setNewTodo(_todo);
    // setTodo({
    //   ...todo,
    //   id: _todo.id,
    //   title: _todo.title,
    //   description: _todo.description,
    //   status: _todo.status,
    // });
  };

  const handleDelete = (_todo) => {
    setDeleteForm(true);
    setNewTodo(_todo);
    // todo.id = _todo.id;
    // todo.title = _todo.title;
    // todo.description = _todo.description;
    // todo.status = _todo.status;
    // setTodo({
    //   ...todo,
    //   id: _todo.id,
    //   title: _todo.title,
    //   description: _todo.description,
    //   status: _todo.status,
    // });
  };

  return (
    <div>
      <hr />
      <span className="boldFont">Total Todos: {todos.length} </span>
      <span>
        ( Active: {todos.filter((_todo) => _todo.status === "Active").length},
        Done: {todos.filter((_todo) => _todo.status === "Done").length} )
      </span>

      <hr />

      <div>
        <FilterByStatus todo_obj={props.todo_obj} />

        {new_todos.map((_todo, index) => {
          _todo.id = index + 1;

          return (
            <div key={_todo.id}>
              <div className="todo-item">
                <div>
                  <span>
                    <span className="boldFont">{_todo.id}. Title: </span>
                    {_todo.title}
                    <span className="boldFont"> (Status: {_todo.status})</span>
                  </span>
                </div>
                <p></p>
                <div>
                  <span className="boldFont">Description: </span>{" "}
                  {_todo.description}{" "}
                </div>
                <p></p>
                <div>
                  <button
                    className="btn btn-primary todo-buttons"
                    onClick={() => handleEdit(_todo)}
                  >
                    Edit
                  </button>
                  <button
                    className="btn btn-danger todo-buttons"
                    onClick={() => handleDelete(_todo)}
                  >
                    Delete
                  </button>
                </div>
                {edit && <ModalForm todo_obj={props.todo_obj} />}
                {deleteForm && (
                  <DeleteForm
                    todo_obj={props.todo_obj}
                    deleteForm={deleteForm}
                    setDeleteForm={setDeleteForm}
                  />
                )}
              </div>
              <br />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default TodoList;
