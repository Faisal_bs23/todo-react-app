import React from "react";
import { Button, Modal } from "react-bootstrap";

const DeleteForm = (props) => {
  const { setDeleteForm } = props;
  const { todo, setTodo, todos, setTodos } = props.todo_obj;

  return (
    <div>
      <Modal show={true} animation={false}>
        <Modal.Body>
          <p>
            Are you sure you want to delete{" "}
            <span className="boldFont">{todo.title}</span>?
          </p>
          <hr />
          <Button
            className="btn btn-danger todo-buttons"
            onClick={() => {
              setTodos(todos.filter((_todo) => _todo.id !== todo.id));
              setTodo({});
              setDeleteForm(false);
            }}
          >
            Delete
          </Button>
          <Button
            className="todo-buttons"
            onClick={() => {
              setDeleteForm(false);
              setTodo({});
            }}
          >
            Cancel
          </Button>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default DeleteForm;
